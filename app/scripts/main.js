'use strict';

// ready
$(document).ready(function() {


    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
    });
    // adaptive menu

    // slider
    $('.single-item').slick({
        arrows: true,
        dots: true
    });
    $('.single-item-banner').slick({
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000
    });
    $('.carousel-item').slick({
        centerMode: false,
        infinite: true,
        centerPadding: '20px',
        slidesToShow: 3,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: true,
                    //centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.carousel-item1').slick({
        centerMode: false,
        infinite: true,
        centerPadding: '20px',
        slidesToShow: 4,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: true,
                    //centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.gallery-item').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    arrows: false
                }
            }
        ]
    });
    $('.about-item').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        variableWidth: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.circle-item').slick({
        //slidesToShow: 4,
        slidesToScroll: 1,
        slidesPerRow: 4,
        rows: 2,
        infinite: true,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesPerRow: 2,
                    rows: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesPerRow: 1,
                    arrows: false,
                    centerMode: false,
                    rows: 2
                }
            }
        ]
    });
    $('.slider-for').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        centerMode: true,
        asNavFor: '.slider-nav',
        responsive: [
            {
                breakpoint: 1249,
                settings: {
                    slidesToShow: 2,
                    arrows: true
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    arrows: true,
                    centerPadding: '0'
                }
            }
        ]
    });
    $('.slider-nav').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: false,
        variableWidth: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1249,
                settings: {
                    arrows: false,
                    slidesToShow: 7
                }
            },
            {
                breakpoint: 991,
                settings: {
                    arrows: false,
                    slidesToShow: 7
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.product-catalog--js').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        centerMode: false,
        asNavFor: '.product-catalog--item-js'
    });
    $('.product-catalog--item-js').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-catalog--js',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
    // slider


    // animation
    $('.animated').appear(function() {
        var elem = $(this);
        var animation = elem.data('animation');

        if ( !elem.hasClass('visible') ) {
            var animationDelay = elem.data('animation-delay');
            if ( animationDelay ) {
                setTimeout(function(){
                    elem.addClass( animation + " visible" );
                }, animationDelay);

            } else {
                elem.addClass( animation + " visible" );
            }
        }
    });
    // animation


    // select
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // fancybox
    $('.imageLink').magnificPopup({
        type:'image',
        gallery: {
            enabled: true
        }
    });
    // fancybox

    //tabs
    $(function() {
        $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
            return false;
        });
    });
    //tabs



});
// ready

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 990) {
    //accordion
    $('.accordion__item').click(function () {
        $(this).toggleClass('active').next().toggleClass('active');
    });
    //accordion
} else {
    if ($(".top-head").length) {

        var header = $('.top-head');
        var range = 200;

        $(window).on('scroll', function () {

            var scrollTop = $(this).scrollTop();
            var offset = header.offset().top;
            var height = header.outerHeight();
            offset = offset + height / 2;
            var calc = 1 - (scrollTop - offset + range) / range;

            header.css({'opacity': calc});

            if (calc > '1') {
                header.css({'opacity': 1});
            } else if (calc < '0') {
                header.css({'opacity': 0});
            }

        });
    }
}
// mobile sctipts